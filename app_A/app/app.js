const express = require("express");
const app = express();
const port = 3001;

app.set("port", port);
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});
app.get("/*", (req, res) => {
  res.send("A~~ &#10004;");
});

app.listen(port, () => console.log("Listening on", port));
module.exports = app;
